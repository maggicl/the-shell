---
layout: page
category-page: scripts
category-title: Scripting
tags: array variable many index selector
author: Marco Tereh
title: Arrays
previous-page: pages/scripts/3-parameter_expansion.html
next-page: pages/scripts/5-for-loop.html
---

There is a special kind of <a href="variables.html">variable</a>, called an array.<br>

While variables hold a single value, arrays hold many. They could be called a list of variables.
The simplest way to create an array is using this format:

{% highlight bash %}
  names=("Anna" "Bob" "Clarice" "Dee")
{% endhighlight %}

This will create an array, called <code>names</code>, which holds 4 values.
The values in the array can be accessed using the syntax

{% highlight bash %}
  ${names[SELECTOR]}
{% endhighlight %}

where SELECTOR is the selector associated to the desired element. There are two kinds of
arrays, depending on what kind of selector is used.<br>

<code>names</code> is an <i>indexed</i> array, meaning the selector is a number.
Since we didn't specify any when we defined it, <code>names</code>' selectors start at
0 and count up from there.<br>

Thus we can access the string <code>"Anna"</code> using <code>names[0]</code>.
In the same way we use <code>names[1]</code> to access <code>"Bob"</code> and so on.<br>

If we want to specify a particular index for our values when creating the array
we can do it like this:

{% highlight bash %}
  names=([1]="Anna" [2]="Bob" [5]="Ernie" [12]="Luigi")
{% endhighlight %}

As you might have guessed, this will create an array with <code>"Anna"</code> at index 1,
<code>"Bob"</code> at index 2, <code>"Ernie"</code> at index 3 and so on.<br>

If our indices are all sequential and we just want to change the starting index, we can use

{% highlight bash %}
  names=([12]="Luigi" "Mario" "Nate")
{% endhighlight %}

Which will create an array with <i>"Luigi"</i> at index 12,
<i>"Mario"</i> at index 13 and <i>"Nate"</i> at index 14.<br>
An indexed array's selector can also be negative, which will start counting from the end,
so <i>${names[-1]}</i> means <i>"Nate"</i>, <code>${names[-3]}</code> is
<i>"Luigi"</i> and so on.<br>

The other kind of array is an <i>associative</i> array.
In an associative array, the values are not mapped to a number but to some other string.<br>

Here's an example of an associative array:

{% highlight bash %}
  colors=([white]="#FFFFFF" [black]="#000000" [red]="#FF0000" [yellow]="#00FFFF")
{% endhighlight %}

In this case it is not possible to omit the selector as there is no logical word
that "follows" white. To access its members, we use the same syntax but write a string
instead of a number as the selector.<br>
Arrays, both kinds, can also be modified after creation. We can use

{% highlight bash %}
  colors[blue]="#0000FF"
{% endhighlight %}

to add an element to the array (or modify it if it exists already) and

{% highlight bash %}
  unset colors[black]
{% endhighlight %}

to remove one.<br>

The <code>@</code> and <code>*</code> selectors are special - they cannot be written to but
reading from them will return <b>all</b> the values in the array, separated by spaces.
Similarly, <code>${!colors[@]}</code> is a list of
all the selectors (or indices) that were assigned a value.<br>

<a href="parameter_expansion.html">Parameter Expansion</a> works on indexed arrays as well,
much like it works on strings, but it manipulates the members of the array instead of characters.
For example:

{% highlight bash %}
  ${names[@]: 13}
{% endhighlight %}

is equivalent to <code>"Mario" "Luigi"</code>, while

{% highlight bash %}
  ${#names[@]}
{% endhighlight %}

counts the number of elements in the array, in this case 3.<br><br>

Further reading: <a href="http://www.gnu.org/software/bash/manual/bashref.html#Arrays">
the bash reference manual</a>
