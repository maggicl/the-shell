# Bonus 1

## Authors
Claudio Maggioni and Joey Bevilacqua

## Running the program
This program is built with rust. In order to run it, a rust toolchain needs to be installed.
Run the program with `cargo run`, providing as first argument the directory with the jekyll
output. For example, in order to run the program from this directory, run:

```
cargo run ../site/_site/
```
